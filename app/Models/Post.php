<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categoria;
use App\Models\Comentario;

class Post extends Model
{
    public $timestamps = false;
    use HasFactory;


    public static $createRules = [
        'titulo' => 'required|unique:posts,titulo|max:150',
        'categorias_id' => 'required',
        'contenido' => 'required'
    ];

    public static function updateRules($id){
        return [
            'titulo' => 'sometimes|min:1|unique:posts,titulo,'.$id.'|max:150',
            'categorias_id' => 'sometimes|min:1',
            'contenido' => 'sometimes|min:1'
        ];
    } 

    public static function categoriasIsNumericAndInt($validator,$categorias_id){
        if(is_numeric($categorias_id) && is_int($categorias_id) == 1){
            return true;
        }else{
            $validator->after(function ($validator) {
                $validator->errors()->add('categorias_id', 'El campo de categoría debe ser un número entero.');
            });
            return false;
        }
    }

    public static function categoriaExist($validator,$id){
        if(!empty(Categoria::find($id))){
            return true;
        }else{
            $validator->after(function ($validator) {
                $validator->errors()->add('categorias_id', 'No existe una categoría asociada al campo categorias_id enviado');
            });
            return false;
        }
    }


    public static $messageCreateRules =
    [
        'titulo.required' => 'El campo título es requerido.',
        'titulo.unique' => 'Ya existe una post con el título digitado.',
        'titulo.max' => 'El campo título puede ser máximo de 150 caracteres.',
        'categorias_id.required' => 'El campo de categoría es requerido.',
        'contenido.required' => 'El contenido es requerido'
    ];

    public static $messageUpdateRules =
    [
        'titulo.min' => 'El campo título no puede ser vacío.',
        'titulo.unique' => 'Ya existe una post con el título digitado.',
        'titulo.max' => 'El campo título puede ser máximo de 150 caracteres.',
        'categorias_id.min' => 'El campo de categoría no puede ser vacío.',
        'contenido.min' => 'El campo contenido no puede ser vacío.'
    ];




    public function categoria()
    {
        return $this->hasOne(Categoria::class, 'id', 'categorias_id');
    }

    public function comentarios(){
        return $this->hasMany(Comentario::class,'posts_id','id');
    }
}
