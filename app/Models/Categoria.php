<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Categoria extends Model
{
    public $timestamps = false;
    use HasFactory;

    public static $createRules = [
        'nombre' => 'required|unique:categorias,nombre|max:150'
    ];

    public static function updateRules($id){
        return [
            'nombre' => 'sometimes|min:1|unique:categorias,nombre,'.$id.'|max:150'
        ];
    } 

    public static $messageCreateAndUpdateRules =
    [
        'nombre.required' => 'El campo nombre es requerido.',
        'nombre.unique' => 'Ya existe una categoría con el nombre digitado.',
        'nombre.max' => 'El campo nombre puede ser máximo de 150 caracteres.',
        'nombre.min' => 'El campo nombre no puede ser vacío.'
    ];



    public function posts()
    {
        return $this->hasMany(Post::class, 'categorias_id', 'id');
    }

}
