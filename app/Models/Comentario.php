<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Comentario extends Model
{

    public $table = 'comentarios';

    public $timestamps = false;
    use HasFactory;


    public static $createRules = [
        'contenido' => 'required|max:500',
        'posts_id' => 'required'
    ];

    public static $updateRules = [
        'contenido' => 'sometimes|min:1|max:500',
    ];

    public static $messageCreateRules =
    [
        'contenido.required' => 'El campo contenido es requerido.',
        'posts_id.required' => 'El campo posts_id es requerido',
        'contenido.max' => 'El campo contenido puede ser máximo de 500 caracteres.'
    ];

    public static $messageUpdateRules =
    [
        'contenido.max' => 'El campo contenido puede ser máximo de 500 caracteres.',
        'contenido.min' => 'El campo contenido no pude ser vacío.'
    ];


    public static function postIsNumericAndInt($validator,$posts_id){
        if(is_numeric($posts_id) && is_int($posts_id) == 1){
            return true;
        }else{
            $validator->after(function ($validator) {
                $validator->errors()->add('posts_id', 'El campo posts_id debe ser un número entero.');
            });
            return false;
        }
    }


    public static function postExist($validator,$id){
        if(!empty(Post::find($id))){
            return true;
        }else{
            $validator->after(function ($validator) {
                $validator->errors()->add('posts_id', 'No existe un post asociado al campo posts_id enviado');
            });
            return false;
        }
    }

    public function post(){
        return $this->hasOne(Post::class,'id','posts_id');
    }
}
