<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;

class ComentarioController extends Controller
{
    public function create(Request $request)
    {
        $comentario_request = $request->json()->all();

        if(count($comentario_request) == 2){

            $validator = Validator::make($request->json()->all(), Comentario::$createRules,Comentario::$messageCreateRules);
            Comentario::postExist($validator,!empty($comentario_request['posts_id']) ? $comentario_request['posts_id'] : '');
            Comentario::postIsNumericAndInt($validator,!empty($comentario_request['posts_id']) ? $comentario_request['posts_id'] : '' );

            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()],406);
            }

            $crear_comentario    =   new Comentario(); 
            $crear_comentario->contenido   =   $comentario_request['contenido'];
            $crear_comentario->posts_id   =   $comentario_request['posts_id'];
            $crear_comentario->fecha_creacion   =   date("Y-m-d H:i:s");
            $crear_comentario->fecha_actualizacion   =   date("Y-m-d H:i:s");
            $crear_comentario->save();

            return response()->json(['mensaje' => 'Se ha creado el comentario exitosamente'],201);
            
        }else{

            return response()->json(['error' => 'Verifica la cantidad de parámetros. Mínimo y máximo, 2 parametro'],406);
        }
    }

    public function update(Request $request, $id)
    {
        $comentario = Comentario::find($id);
        if(!empty($comentario)){

            $columns = Schema::getColumnListing($comentario->getTable());
            $comentario_request = $request->json()->all();
            $errors = new MessageBag();
            foreach ($comentario_request as $key => $value) {
                if(!in_array($key,$columns)){
                    $errors->add($key, 'La propiedad '.$key.' no es parte del modelo');
                }
            }

            if(count($errors) > 0){
                return response()->json(['schema_errors'=> $errors],406);
            }
            
            if(count($comentario_request) == 1){
    
                $validator = Validator::make($request->json()->all(), Comentario::$updateRules,Comentario::$messageUpdateRules);
                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()],406);
                }

                $comentario->contenido = !empty($comentario_request['contenido']) ? $comentario_request['contenido'] : $comentario->contenido;
                $comentario->fecha_actualizacion   =   date("Y-m-d H:i:s");
                $comentario->save();
        
                return response()->json(['mensaje' => 'Se ha actualizado el comentario exitosamente'],200);
            }else{
                return response()->json(['error' => 'Verifica la cantidad de parámetros. máx. 1 parametro'],406);
            }
        }else{
            return response()->json(['error' => 'El comentario con el id '.$id.' no existe'],406);
        }
    }

    public function destroy($id)
    {
        $comentario = Comentario::find($id);
        if(!empty($comentario)){
            $comentario->delete();
            return response()->json(['mensaje' => 'Se ha borrado el comentario exitosamente'],200);
        }else{
            return response()->json(['error' => 'El comentario con el id '.$id.' no existe'],406);
        }
    }

    public function ver($id)
    {
        $comentario = Comentario::with('post')->find($id);
        if(!empty($comentario)){
            return response()->json(['comentario'=>$comentario],200);
        }else{
            return response()->json(['error' => 'El comentario con el id '.$id.' no existe'],406);
        }
    }

    public function verTodo()
    {
        $comentarios = Comentario::with('post')->get();
        return response()->json(['comentarios'=>$comentarios],200);
    }
}
