<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;

class PostController extends Controller
{
    public function create(Request $request)
    {
        $post_request = $request->json()->all();

        if(count($post_request) == 3){

            $validator = Validator::make($request->json()->all(), Post::$createRules,Post::$messageCreateRules);
            Post::categoriasIsNumericAndInt($validator,$post_request['categorias_id']);
            Post::categoriaExist($validator,$post_request['categorias_id']);

            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()],406);
            }

            $crear_post    =   new Post(); 
            $crear_post->titulo   =   $post_request['titulo'];
            $crear_post->contenido   =   $post_request['contenido'];
            $crear_post->categorias_id   =   $post_request['categorias_id'];
            $crear_post->fecha_creacion   =   date("Y-m-d H:i:s");
            $crear_post->fecha_actualizacion   =   date("Y-m-d H:i:s");
            $crear_post->save();

            return response()->json(['mensaje' => 'Se ha creado el post exitosamente'],201);
            
        }else{

            return response()->json(['error' => 'Verifica la cantidad de parámetros. mínimo y máximo 3 parametros'],406);
        }
    }


    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if(!empty($post)){

            $columns = Schema::getColumnListing($post->getTable());
            $post_request = $request->json()->all();

            $errors = new MessageBag();
            foreach ($post_request as $key => $value) {
                if(!in_array($key,$columns)){
                    $errors->add($key, 'La propiedad '.$key.' no es parte del modelo');
                }
            }

            if(count($errors) > 0){
                return response()->json(['schema_errors'=> $errors],406);
            }


            if(count($post_request) > 0 && count($post_request) <= 3){
    
                $validator = Validator::make($request->json()->all(), Post::updateRules($post->id),Post::$messageUpdateRules);
                Post::categoriasIsNumericAndInt($validator,!empty($post_request['categorias_id']) ? $post_request['categorias_id'] : $post->categorias_id);
                Post::categoriaExist($validator,!empty($post_request['categorias_id']) ? $post_request['categorias_id'] : $post->categorias_id);
    
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()],406);
                }
    
                $post->titulo   =   !empty($post_request['titulo']) ? $post_request['titulo'] : $post->titulo;
                $post->contenido   =  !empty($post_request['contenido']) ? $post_request['contenido'] : $post->contenido;
                $post->categorias_id   =   !empty($post_request['categorias_id']) ? $post_request['categorias_id'] : $post->categorias_id;
                $post->fecha_actualizacion   =   date("Y-m-d H:i:s");
                $post->save();
    
                return response()->json(['mensaje' => 'Se ha actualizado el post exitosamente'],201);
                
            }else{
    
                return response()->json(['error' => 'Verifica la cantidad de parámetros. Mínimo 1 parametro y máximo 3 parametros'],406);
            }
        }else{
            return response()->json(['error' => 'El post con el id '.$id.' no existe'],406);
        }
    }


    public function destroy($id)
    {
        $post = Post::find($id);
        if(!empty($post)){
            $post->delete();
            return response()->json(['mensaje' => 'Se ha borrado el post exitosamente'],200);
        }else{
            return response()->json(['error' => 'El post el id '.$id.' no existe'],406);
        }
    }


    public function ver($id)
    {
        $post = Post::with('comentarios')->find($id);
        if(!empty($post)){
            return response()->json(['post'=>$post],200);
        }else{
            return response()->json(['error' => 'El post con el id '.$id.' no existe'],406);
        }
    }


    public function verTodo()
    {
        $post = Post::with('comentarios')->get();
        return response()->json(['posts'=>$post],200);
    }


}
