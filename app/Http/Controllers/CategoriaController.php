<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;

class CategoriaController extends Controller
{

    public function create(Request $request)
    {
        $categoria_request = $request->json()->all();

        if(count($categoria_request) == 1){

            $validator = Validator::make($request->json()->all(), Categoria::$createRules,Categoria::$messageCreateAndUpdateRules);
            
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()],406);
            }

            $create_categoria    =   new Categoria(); 
            $create_categoria->nombre   =   $categoria_request['nombre'];
            $create_categoria->fecha_creacion   =   date("Y-m-d H:i:s");
            $create_categoria->fecha_actualizacion   =   date("Y-m-d H:i:s");
            $create_categoria->save();

            return response()->json(['mensaje' => 'Se ha creado la catergoria exitosamente'],201);
            
        }else{

            return response()->json(['error' => 'Verifica la cantidad de parámetros. máx. 1 parametro'],406);
        }
    }

    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        if(!empty($categoria)){


            $columns = Schema::getColumnListing($categoria->getTable());
            $categoria_request = $request->json()->all();

            $errors = new MessageBag();
            foreach ($categoria_request as $key => $value) {
                if(!in_array($key,$columns)){
                    $errors->add($key, 'La propiedad '.$key.' no es parte del modelo');
                }
            }

            if(count($errors) > 0){
                return response()->json(['schema_errors'=> $errors],406);
            }


            if(count($categoria_request) == 1){
    
                $validator = Validator::make($request->json()->all(), Categoria::updateRules($categoria->id),Categoria::$messageCreateAndUpdateRules);
    
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()],406);
                }
        
                $categoria_request = $request->json()->all();
                $categoria->nombre = !empty($categoria_request['nombre']) ? $categoria_request['nombre'] : $categoria->nombre;
                $categoria->fecha_actualizacion   =   date("Y-m-d H:i:s");
                $categoria->save();
        
                return response()->json(['mensaje' => 'Se ha actualizado la catergoria exitosamente'],200);
            }else{
                return response()->json(['error' => 'Verifica la cantidad de parámetros. máx. 1 parametro'],406);
            }
        }else{
            return response()->json(['error' => 'La categoría con el id '.$id.' no existe'],406);
        }
    }

    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        if(!empty($categoria)){
            try {
                $categoria->delete();
                return response()->json(['mensaje' => 'Se ha borrado la catergoria exitosamente'],200);
            } catch (\Throwable $th) {
                return response()->json(['error' => 'Esta categoría no se puede borrar porque uno o más post hacen uso de ella.'],500);
            }
        }else{
            return response()->json(['error' => 'La categoría con el id '.$id.' no existe'],406);
        }
    }

    public function ver($id)
    {
        $categoria = Categoria::find($id);
        if(!empty($categoria)){
            return response()->json(['categoria'=>$categoria],200);
        }else{
            return response()->json(['error' => 'La categoría con el id '.$id.' no existe'],406);
        }
    }

    public function verTodo()
    {
        $categoria = Categoria::all();
        return response()->json(['categorias'=>$categoria],200);
    }
}
