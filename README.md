
# API REST LARAVEL

Prueba de un API REST para validar el conocimiento en Laravel 💻. Este proyecto esta realizado en la versión 8.75 de Laravel.  





## Autor

- [Juan Camilo Vallejos](www.linkedin.com/in/jcamilovallejos) 👨‍💻


## Instalación en local


```bash
  git clone https://gitlab.com/jcamilovallejos/opperweb.git
  cd opperweb
  composer install
  mv .env.example .env
  nano .env 
```
Cambia el valor de la variable de entorno DB_DATABSE=laravel por el nombre de la base de datos que hayas creado en tu servidor mysql/mariadb local. En mi caso lo dejaré como  opperweb

```bash
    DB_DATABSE=opperweb 
    Ctrl + O 
    Enter
    Ctrl + X
    php artisan migrate --seed
```

👨‍💻 Esto sería todo. Estaría listo para ser testeado a través de Postman. Muchas gracias por la oportunidad. 

    