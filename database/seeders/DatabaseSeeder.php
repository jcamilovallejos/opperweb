<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Categoria;
use App\Models\Comentario;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Categoria::factory(10)->has(Post::factory(5)->has(Comentario::factory()->count(3)))->create();
    }
}
