<?php

namespace Database\Factories;
use App\Models\Categoria;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => wordwrap($this->faker->sentence(6,true),150),
            'contenido' => $this->faker->text(400),
            'fecha_creacion' => $this->faker->dateTimeBetween('-2 years','now',null),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now',null)
        ];
    }
}
