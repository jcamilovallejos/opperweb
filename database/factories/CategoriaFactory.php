<?php

namespace Database\Factories;
use App\Models\Categoria;

use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaFactory extends Factory
{
    protected $model = Categoria::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => wordwrap($this->faker->sentence(1,true),150),
            'fecha_creacion' => $this->faker->dateTimeBetween('-2 years','now',null),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now',null),
        ];
    }
}
