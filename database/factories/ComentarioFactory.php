<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComentarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'contenido' => $this->faker->text(500),
            'fecha_creacion' => $this->faker->dateTimeBetween('-2 years','now',null),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now',null)
        ];
    }
}
